### build image from code 

docker build -t firstimage .

### Pull

docker pull YOUR_DOCKERHUB_NAME/firstimage:your_tag

### run image

docker run -d -p 80:80 firstimage

### commit changes

docker commit [votre-id-de-conteneur] my-new-image

### Tag

docker tag firstimage YOUR_DOCKERHUB_NAME/firstimage

### Push

docker push YOUR_DOCKERHUB_NAME/firstimage

### tips

sometimes you had to log / logout from docker before push new image on docker hub

### get depedancy

composer install

### units test

./vendor/bin/phpunit tests

### docker hub public image

<https://hub.docker.com/r/pedrodocker974/my-php-app-test/tags>
