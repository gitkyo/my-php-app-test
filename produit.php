<!DOCTYPE html>
<html lang="fr">

<?php require("head.php"); ?>

<?php 


//connection a la base de donnée
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "club";


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//on test la connexion
if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
}

//ici on test si le formulaire est validé
if(isset($_POST["description"]) && $_POST["description"] != "" && isset($_POST["idproduit"]) && $_POST["idproduit"] != "" ){


    //on récupere les données du formulaire
    $description = $_POST["description"];

    //on récupere les données du formulaire
    $idProduitt = $_POST["idproduit"];

    //requete
    $sql = "UPDATE produit SET description = '$description' WHERE id = $idProduitt";

    //execution de la requette
    $result = $conn->query($sql);
    
}





//requete poour recuperer les produits
$sql = "SELECT * FROM produit";

//execution de la requette
$result = $conn->query($sql);


//tableau de produits
$tabProduit = array();

//traitement et affichage des réusltats
if ($result->num_rows > 0) {

        //la methode fetch_assoc() permet de parcourir les lignes de la table
        while($row = $result->fetch_assoc()) {

            //ajout des données dans tabProduit
            $tabProduit[] = $row;

        }
} 
else {
        echo "No records has been found";
}

//fermeture de la bdd
$conn->close();

?>


<body>

    <?php require("nav.php"); ?>
    

    <h1>coucou ceci est ma page produits</h1>

    <div class="mesProduits">

    


    <?php 
    //on compte le nbre de produit dans le tableau tabproduit
    $nbreProduit = count($tabProduit);

    //pour chaque produit
    for($i = 0; $i < $nbreProduit; $i++){

        //on affiche le produit
        print('
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="https://images.unsplash.com/photo-1627483262769-04d0a1401487?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Produit '.$tabProduit[$i]["id"].'</h5>                    
                    <p class="card-text">'.$tabProduit[$i]["description"].'</p>
                    <a href="/produit.php?edit=true&id='.$tabProduit[$i]["id"].'" class="btn btn-primary">Edit</a>
                </div>
            </div>
        ');

    }
    
    ?>

    <!-- fin mesProduits -->
    </div>
    <?php
            //test si on est en mode edition
            $isEdit = false;

            //on récupere l'url
            $url= $_SERVER['REQUEST_URI'];    


            //on récupere que le nom de la page 
            $TabUrl = explode("/", $url);
         

            $TabUrl2 = explode("?", $TabUrl[2]);

            //je test s'il y a des parametres
            if(isset($TabUrl2[1]) && $TabUrl2[1] != NULL) {

                //recuperation des parametres
                $TabUrl3 = explode("&", $TabUrl2[1]);

                if( $TabUrl3[0] == "edit=true" ) {
                    $isEdit = true;
                    

                    $idProduitTab = explode("=", $TabUrl3[1]);
                    $idProduit = $idProduitTab[1];
                   
                }
            }
        ?>

        

        <!-- formulaire d'edition d'un produit -->
        <?php if($isEdit) { ?>
            <div class="formProduit container">
                <h2>Formulaire d'edition du produit numero <?php echo $idProduit ?> </h2>
                <form action="produit.php" method="POST">
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <input type="text" class="form-control" name="description" id="description">

                        <input class="invisible" type="text" name="idproduit" value="<?php echo $idProduit ?>">
                    </div>
                    <button type="submit" class="btn btn-primary">Mettre à jour</button>
                </form>
            </div>

        <?php } ?>

        
    <!-- <pre><?php //var_dump($tabProduit);  ?></pre> -->


</body>
</html>